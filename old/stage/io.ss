(module io mzscheme

  (provide (all-defined))
  
  ;; communication routines

  ;; return a pair of ( input . output ) ports
  (define (open-io-device name)
    (let ((io-device
           (call-with-values
               (lambda () 
                 (open-input-output-file name 'append))
             cons)))
      (file-stream-buffer-mode (cdr io-device) 'none)
      io-device))

  ;; false if timeout
  ;; input ports are synchronizable events in plt scheme
  ;; this guarantees read-byte will not block
  (define (port-ready? timeout port)
    (sync/timeout timeout port))


  ;; FIXME: disabled for snot
  (define (--read-byte-progress port timeout progress)
    (buffer-mode 'none)
    
    (let again ()
      (if (port-ready? timeout port)
          (begin
            (buffer-mode 'line)
            (read-byte port))
          (begin
            (progress)
            (again)))))

  (define (read-byte-progress port timeout progress)
    (read-byte port))

  (define (progress) (display "."))

  (define (buffer-mode x)
    (file-stream-buffer-mode
     (current-output-port) x))


  ;; save a tree list with newlines between leaf nodes (darcs friendly)

  (define (write-tree lst . port)
    (define (wrt x) (apply write (cons x port)))
    (define (dsp x) (apply display (cons x port)))
    
    (define (serializable x)
      (or (null? x)
          (string? x) 
          (symbol? x)
          (number? x)
          (char? x)))

    (define (checktree x)
      (if (pair? x)
          (begin
            (checktree (car x))
            (checktree (cdr x)))
          (if (not (serializable x))
              (raise `(cannot-serialize ,x)))))
        

    (checktree lst)
    
    (let wrt-tree ((l lst))
      (cond
       ((list? l) (begin
                    (dsp "(\n")
                    (for-each wrt-tree l)
                    (dsp ")\n")))

       ((pair? l) (begin
                    (wrt l)
                    (dsp "\n")))
       (else
        (wrt l)
        (dsp "\n")))))

  ;; (write-tree '(1 2 (4 5 6 (a . b) (c . d)) 3))




  )