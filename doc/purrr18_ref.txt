
PURRR18 REFERENCE MANUAL




BASIC
-----


interaction words
-----------------

These words are only defined in the interactive console, and can not
be used in forth files. Note that some of them are actually 2 words,
but still count as one command. (for example 'load' and 'start')



\ loading and compiling

load <filename>		\ --        load a forth file
commit                  \ --        commit current assembler buffer to chip
pa 			\ --        print the contents of the assembler buffer
clear			\ --	    clear the contents of the assembler buffer

mark                    \ --        mark the dictionary
empty                   \ --        clear the dictionary from last 'mark'
scrap 			\ --	    scrap project, start from scratch


\ target interaction

ping                    \ --        request and print target identification
cold			\ --	    perform PIC cold reset 


\ printing target data

p                       \ byte --   print byte on target stack
pbyte                   \ byte --   same as 'p', but print in HEX
ts                      \ --        print target stack (unsigned bytes)
tss                     \ --                       ... (signed bytes)
tsh			\ --                       ... (hex bytes)
_ts _tss _tsh           \ --        same as 'ts tss tsh', but print words


\ running programs

<word>			\ ... -- ...  run a target word and wait till done
start <word>		\ ... -- ...  run a target word, but don't wait till done
<number>                \ -- n        load a number on the target stack

\ stack manipulation    

dup      \ a -- a a     duplicate a number on the stack
drop     \ a --         discard a number on the stack
+        \ a b -- a+b	add two numbers 
	       	  	similar: - and or xor

<<       \ a -- b       shift bits left, rightmost is zero (carry gets falloff)
>>       \ a -- b       shift bits right, leftmost is zero (carry gets falloff)
rot<<	 \ a -- b       rotate bits left
rot>>    \ a -- b 	rotate bits right



\ variable manipulation

!        \ val addr --  store value in address (variable)
@        \ addr -- val  fetch value from address (variable)
+!       \ val addr --  add val to values stored in address (variable)
                        similar: -! and! or! xor!


creating new named words
------------------------

: <word>   \ --	         start a new word definition
;          \ --          end a word definition (return to caller)

variable  <word>  \ --     create a one byte variable
2variable <word>  \ --     create a two byte variable

constant  <word> \ a --    create a one byte constant
2constant <word> \ a b --  create a two byte constant





compilation words
-----------------

Some words only make sense in compile mode. Most of them perform some
kind of control (jumping / looping) operation.


for 	   \ n --       start for loop
next       \ --         end for loop (for .. next need to match up)

begin      \ --         start (un)conditional loop
again      \ --         jump back to matching begin
until      \ ? --	jump back to begin if false, continue if true

not        \ ? -- ?	invert condition

if         \ ? --      	conditional execution: if ... else ... then
else       \ --         start of alternative
then       \ --         end of conditional






condition words
---------------

These can be combined with words that expect a flag, represented as ?
in this documentation above.

high?	\ byte bit -- ?	   is the bit set?
low?    \ byte bit -- ?	   is the bit clear?
=?      \ a b -- a b ?     are 2 numbers equal?
>?      \ a b -- a b ?     is a larger than b?
<?      \ a b -- a b ?     is a smaller than b?
>=?     \ a b -- a b ?     is a greater than or equal to b?
<=?     \ a b -- a b ?     is a less than or equal to b?
z?	\ -- ?	     	   last operation returned zero
c?	\ -- ?		   last operation produced carry


misc
----

high    \ byte bit --      set bit
low     \ byte bit --      clear bit





hooks
-----

run-hook   \ id --         run the hook with specified id
->         \ id --         set the hook with specified id to following code  


ADVANCED
--------


macros
------

You can create new macros by enclosing ordinary forth definitions in
between 'macro' and 'forth' words. If you are confused by this, simply
don't use it.



misc advanced
-------------

these use 'intermediate state' and as such are considered low
level. if you need these, try to wrap them in a word.

rot>>c	 \ a -- b	rotate bits left through carry flag
rot<<c	 \ a -- b	rotate bits right through carry flag
clc	 \ --		clear carry flag
stc	 \ --		set carry flag




