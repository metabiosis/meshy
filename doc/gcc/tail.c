int main(int argc, char **argv){
    return foo(0,0);
}

int foo(int a, int b){
    if ((a * b) == 123)
	return 0;
    return bar(a+2,b+3);
}

int bar(int a, int b){
    return foo(a+5,b+7);
}
