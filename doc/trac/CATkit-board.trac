[[TOC(CATkit-proto, CATkit-board, CATkit-huds, CATkit-howto )]]
= The CATkit board =

CATkit is an open hardware project. The kit consists of a printed circuit board plus components, to be used as a standalone interactive Forth computer. This board serves as a teaching platform for physical computing workshops, and as a tool for artists looking for a simple system to create stand alone objects with a couple of knobs and switches or other sensors.


== CATkit software ==

CATkit can be programmed using the language [wiki:purrr Purrr], which is a dialect of the Forth programming language. Forth is a very simple yet powerful programming language. Due to its interactive nature, it is very easy to pick up. 

[[Image(CATkit-1-400x300.jpg, nolink, float:left)]]
[[Image(CATkit-2-400x300.jpg, nolink, float:right)]]
[[Image(CATkit-3-400x300.jpg, nolink, float:left)]]
[[Image(CATkit-4-400x300.jpg, nolink, float:right)]]

== CATkit hardware  ==

The CATkit board is equipped with the following:

    * Microchip PIC18LF1320 processor, decoupling capacitor and protection diode.
    * Serial/Power port based on the FTDI TTL usb serial cable
    * ICD2 port for low level in-circuit programming and debugging
    * Room for 5 analog pots, or 5 digital/analog sensors
    * Audio out (line voltage or differential direct speaker drive)
    * Reset switch + 3 extra switches
    * Board designed with gEDA + full project files available upon request