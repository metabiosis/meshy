\ vectored execution. this needs execute.f

\ the vector representation used BYTE ADDRESSES, however, this is
\ abstract.

\ perform execution given a variable address: extra level of indirection
macro    
: 2@ dup >m @ m> 1 + @ ;
: 2! dup >m 1 + ! m> ! ;    
: invoke 2@ execute/b ;
: ->  do-arrow stub ;    
forth

\ implementing this word as a macro leads to too much code, so i'm
\ using indirect addressing. 
: do-arrow      0 a!! rl @ !a+ rh @ !a+ pop ;

    