\ chip constants

load p18f2550-const.f

\ chip config macros for core code

macro
: serial-div 4 ;

: init-chip
    3-output ;

: init-serial
    TRISC 7 high TRISC 6 low  \ pin tristate
    init-serial-baud-16 ;     \ 16 bit timer using serial-div defined above

forth
