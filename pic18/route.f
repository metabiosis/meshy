\ -*- forth -*-
: _route/e              \ clip to last
    min
: _route 
    rot<< dup TOSL +!	\ can use rot since LSB is ignored..
    1 and TOSH ++! ;	\ ..which enables us to use it here
