\ chip constants
load p18f2620-const.f

macro

\ Chip supports 16 and 64 bit dividers. We're using an 8 bit
\ resolution divider, which has a constant 16 factor.
  
: serial-div 16 ;

\ Chip config macros for core code

: init-chip
    -1 LATC !        \ prevent TX pin glitch
    #x70 OSCCON ! ;  \ internal oscillator at 8MHz



\ Detect the presence of a serial cable by detecting an IDLE state
\ (high) on RX (RC7). Tie the pin low using 100k to default to BREAK
\ with no cable connected, or jumper it.

\ This macro is only valid AFTER initializing the serial port, which
\ sets the inputs to digital.
    
: debug-serial?
    PORTC 7 high? ;
    
: init-serial
    \ see DS39626C-page 211
    #b11000000 TRISC or!         \ pin config
    init-serial-baud-8           \ like other pics
    ;

\ A/D converter
\ DS39626C p. 224

\ Ports are configured as analog using a range, not by individual
\ ports. This a bit awkward, so do this manually in the app.


    
forth

  