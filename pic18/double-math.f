\ double word math 8 -> 16 bit


\ some 16bit math routines. i don't have time for it now, but at this
\ point, a 16 bit forth would come in handy. all math on periods is
\ unsigned.
    
: _dup    over over ;

: _2drop _drop
: _drop  drop drop ;
    
: _nip    >x >x drop drop x> x> ;    
: _<<     clc 2nd rot<<c! rot<<c ;
: _>>     >> 2nd rot>>c! ;        \ unsigned
: _2/     2/ 2nd rot>>c! ;        \ signed

: _+               \ ( a b c d ) ( )
    >x swap>x      \ ( a c )     ( b d )
    + x> x- @ ++ ;

: _-
    >x swap>x      \ ( a c )     ( b d )
    - x> x- @ -- ;

: _invert
    WREG comf d=reg
    2nd comf d=reg ;


\ these leave carry flag intact, but not the zero flag
: _1+
    dup >x
    1 movlw INDF0 addwf  d=reg
    0 movlw INDF1 addwfc d=reg
    drop x> ;
    
: _1-
    dup >x
    -1 movlw INDF0 addwf  d=reg
             INDF1 addwfc d=reg
    drop x> ;
    


: _negate
    _invert
    _1+ ;

    
\ revised routines

: 2hi>x >x swap>x ;
: _and  2hi>x and x> x> and ;
: _xor  2hi>x xor x> x> xor ;
: _or   2hi>x or  x> x> or ;


\ combine low parts of 16 bit words into one 16bit word
: _lohi >x drop drop x> ;    

macro
: lohi val | val val 8 >>> ;
: 2@   dup >m @ m> 1 + @ ; \ addr -- lo hi
: 2!   dup >m 1 + ! m> ! ; \ lo hi addr --   
forth

