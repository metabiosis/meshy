;; central name space store
(module ns mzscheme

  ;; If you need a purely functional structure, use dict.ss

  (require "hhash.ss"
           "list-utils.ss")

  
  (provide ns-ref
           ns-set!
           ns-remove-all!
           ;; ns-new
           ns-ls
           ns-wordlist
           ns-for-each
           ls)

  (define-syntax ls
    (syntax-rules ()
      ((_ . path)
       (ns-ls 'path))))


  ;; the namespace is organized as a tree (or acyclic graph). each
  ;; node is either a hash table or a word instance.

  (define *root* (make-hhash))
  (define (ns-ref  . a)    (apply hhash-ref  *root* a))
  (define (ns-set! . a)    (apply hhash-set! *root* a))
  (define (ns-remove-all! . a) (apply hhash-remove-all! *root* a))
  ;; (define (ns-new . a)   (apply hhash-new  *root* a))

  (define (make-notfound path)
    (lambda ()
      (error 'not-found "~a" path)))

  (define (ns-ls path)
    (define lst '())
    (ns-for-each path
                 (lambda (key . ignored)
                   (push! lst key)))
    lst)
     
  (define (ns-wordlist namespaces)
    (apply append
           (map (lambda (ns)
                  (ns-ls ns))
                namespaces)))
  
  ;; Visit all nodes in the dictionary tree.
  
  (define (ns-for-each  path     ;; start space
                        visit)

    (hhash-for-each
     (ns-ref path (make-notfound path)) visit))
  
  )