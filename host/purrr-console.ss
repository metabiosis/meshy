;; a shrink-wrapped console for Purrr

(module purrr-console mzscheme
  (require "console.ss"
           "rpn-eval.ss"
           "pic18.ss"
           "composite.ss"
           "prj.ss"
           )

  (provide (all-defined)
           rpn-compile)

  ;; Convenience

  (define *state* (init-target))

  (define-syntax rep 
    (syntax-rules ()
      ((_ delegate)
       (lambda (str)
         (set! *state*
               (delegate *state* eval str))))))

  (define live  (rep live-rep))
  (define macro (rep macro-rep))
  (define prj   (rep prj-rep))

  (current-directory "~/brood")
  (compositions (prj) prj:
                (revert "~/brood/prj/CATkit/monitor.state"
                        load-state! revert-macros))


  )