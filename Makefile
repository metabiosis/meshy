PREFIX = /usr/local


all: bootstrap pic18
	make -C host all

bootstrap:
	make -C prj/purrr18 bootstrap
	make -C prj/USBPicStamp bootstrap
	make -C prj/forthtv bootstrap
	make -C prj/CATkit bootstrap
	chmod +x bin/*
	chmod +x doc/trac/update

clean:
	rm -f *~ */*~
	make -C host clean

emacs-install:
	install -m 644 emacs/cat.el $(PREFIX)/share/emacs/site-lisp/

snapshot:
	bin/dist-snapshot

.PHONY: pic18
pic18:
	mzc -k host/pic18.ss
