\ Bit Banged Framed Byte Receiver/Transmitter

\ RECEIVE + TRANSMIT BUFFERS
    
\ This defines a couple of macros which we'll instantiate
\ specialized.

\ NOTE: i'd like to automate this specialization a bit more, but i
\ don't know how to do it yet. It requires higher order macros that i
\ don't know yet how to implement. So until that time this code is
\ specialized for ONLY ONE rx/tx in the global namespace, and it's NOT
\ PREFIXED! I decided to prefix the async serial words instead, so at
\ least they don't clash.


load buffer.f
    

macro
: tx         #x100 tx-r/w #x0F ;  \ put buffers in RAM page 1
: rx         #x110 rx-r/w #x0F ;        

: tx-ready?  tx-empty>z z? not ;  
: rx-ready?  rx-empty>z z? not ;  
: tx-room?   tx-room>z z? ;
: rx-room?   rx-room>z z? ;
forth

macro
: a[ al @ >x ah @ >x ;
: ]a x> ah ! x> al ! ;    
forth

  
2variable tx-r/w
2variable rx-r/w  
  
: tx-empty>z tx buffer.empty>z ;  
: rx-empty>z rx buffer.empty>z ;  
: rx-room>z  rx buffer.room>z ;  
: tx-room>z  tx buffer.room>z ;  

\ execute in ISR context, so a needs to be saved.    
: >rx      a[ rx buffer.write ]a ;
: tx>      a[ tx buffer.read  ]a ;
: >tx      a[ tx buffer.write ]a ;  
: rx>      a[ rx buffer.read  ]a ;
    
: clear-tx tx buffer.clear ;
: clear-rx rx buffer.clear ;

: rx-size  rx buffer.size ;
: tx-size  tx buffer.size ;    

\ test
    
\ : tx-size  tx buffer.size ;    

: wait-tx-room
    begin tx-room? until ;
: wait-rx-ready
    begin rx-ready? until ;

    

\ RECIEVER - TRANSMITTER 

\ RX/TX FRAME AUTOMATA

\ a simple trick allows to test for 2 conditions: carry and zero,
\ which is enough to encode start/data/stop phases in a single number.
	
variable phase 
variable shift
variable tx-delay
variable tx-delay-count
  
macro
: phase-next phase 1-! ;
: start? c? not ;               \ 0 -> -1
: stop? z? ;                    \ 1 -> 0
: phase->data 9 phase ! ;
: phase->start 0 phase ! ;    

\ FIXME: there's a conditional instruction for this: 1+! z? can be
\ optimized.
: rx/tx-busy?
    phase 1-!
    phase 1+! z? not ;  

forth

: tx-next \ -- bit
    phase-next
    
    stop? if
        tx-debug-1
        1 ;
    then

    tx-debug-0
    
    start? if

\         tx-delay-count 1-! c? if
\             phase->start \ no 0 -> -1 transition:
\             1 ;          \ delay transmission
\         then
        
\         tx-delay @
\         tx-delay-count ! \ restore counter and proceed
            
        tx-ready? if
	    tx> shift !  \ put next byte in shift register
	    phase->data  \ start state machine
	    0 ;          \ start bit = 0
	then
	phase->start     \ no byte? retry next time
	1 ;              \ remain idle
    then

    shift @ 1 and        \ DATA
    shift rot>>!         \ lsb first
    ;

: rx-next \ bit --
    
    phase-next
    
    start? if
	c! c? not if     \ startbit?
	    phase->data ;
	then
	phase->start ;
    then
    
    stop? if
        \ drop 1   \ DEBUG
        c!
        c? if             \ check stop bit
            rx-room? if
                shift @ >rx  \ store received byte
            then 
        then ;
    then
    
    c! shift rot>>c!     \ lsb first
    ;

: init-rx/tx
    clear-rx
    clear-tx
    0 phase !
    0 tx-delay !
    0 tx-delay-count !
    ;
    