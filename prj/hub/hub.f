\ FRAMED BUFFERED RX/TX
load frame.f
    
\ ISR STUFF
load interrupts.f

\ BUS CONTROL
load bus.f    
    
    
\ PULSE MODULATOR + MULTIPLEXING

load e2.f    

: bus-pow|sw
    bus-pow       \ set pin state first
    switch-rx/tx  \ check for rx/tx switch
    ;  
    
: tx-bit  tx-next bus-write ;
: rx-bit  bus-read rx-next ;
    
    
: e2-next
    e2-machine

	\ TX       \ RX
	bus-0      ; bus-0      ;
	bus-pow|sw ; bus-pow|sw ;
	tx-bit     ; bus-Z      ; 
	           ; rx-bit     ;

	

\ TEST & BOOT

macro
: tx-debug-1    LATB 1 high ;
: tx-debug-0    LATB 1 low ;
: tx-debug-init TRISB 1 low ;    
forth



\ these switch the receiver/transmitter on and off. precondition:
\ slave devices need to be in powered state (bus-power) AND the framed
\ receiver is initialized  
    
    
: e2-on
    cli             \ switch off interrupts during init
    interrupt-init
    tmr2-init
    tmr2-on
    2 e2-next-rx!   \ does it matter?
    sti ;

: hub-init
    init-rx/tx    \ resets receive and transmit buffers
    0 bus-client
    bus-reset
    e2-on ;

: hub-reset
    e2-off
    hub-init ;
    
: e2-off
    tmr2-off                    \ switch off interrupt
    begin tmr2-if high? until   \ wait till next phase
    bus-pow ;                   \ permanently switch on


: ack-of-death
    6              \ interpreter ack command
: spam    
    hub-init
    24 tx-delay !  \ room for reply
: tx-spam \ byte --
    tx-debug-init
    begin
        async.rx-ready? if ; then
        tx-room?
    until
    dup >tx tx-spam ;

macro
: host-ready?   async.rx-ready? ;
: target-ready? rx-ready? ;   
forth  

: host>   begin host-ready?   until async.rx> ;
: target> begin target-ready? until rx> ;
: >target >tx ;
: >host   async.>tx ;


\ Smart debug repeater: forward all traffic from target -> host, but
\ keep an eye on the host -> target traffic to find hub commands.
    
: repeater
    begin
        host-ready?     if host> hub-interpret then
        target-ready?   if target> >host then
    again

\ It is assumed that during a host -> target tranfer, no bytes arrive
\ from the target, so we don't need to poll.
    
: host>target host> >target ;
    
\ To synchronize to the protocol we need to know the size of the host
\ -> target messages. The types:
    
: 0_ >target ;
: 1_ >target host>target ;
: 2_ >target host>target host>target ;
: n_ >target
    host> dup >target      \ transfer count
    for host>target next ; \ transfer bult


\ Hub's interpreter



        
: client-set
    drop e2-off
    host> bus-client
    e2-on ;
    
: client-on    drop ;
: client-off   drop ;
: hub-debug    drop interpreter ;    

: hub-interpret
    dup route
        
        \ patch through
        0_ ; 1_ ; 0_ ; 2_ ; 
        2_ ; 2_ ; 0_ ; 0_ ; 
        1_ ; 1_ ; n_ ; n_ ; 
        0_ ; 0_ ; 0_ ; 0_ ;

        \ hub commands
        client-set ; hub-debug ;
        client-on  ; client-off ;


    
macro  
: install

    #x0200 org-push hub-init repeater \ ack-of-death
    exit org-pop

    \ compile isr inline
    #x0208 org-push

    ' tmr2-if ' e2-next ack/dispatch
    reset \ spurious interrupt

    org-pop ;
  
forth

    
    
