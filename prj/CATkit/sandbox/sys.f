\ sys.f
\ contains the different sync settings and some handy utils

\ SYNC

: fast-sync   5 sync ;   \ used for fast modulation
: mod-sync    9 sync ;   \ used for slow modulation
: note-sync  10 sync ;   \ 1/32 th notes
: fast-note-sync 8 sync ;
: dyn-sync 1 + 1 for << next for 8 sync next ;

    
\ CONTROL

\ n -- \ retrigger the current sample for n notes
\ snd n -- \ play the sample snd for n notes    
: notes
    note-sync bang
     for note-sync next 
     silence ;

: fast-notes
    fast-note-sync bang
     for fast-note-sync next 
     silence ;
     
\ SYSTEM
     
: restart
    2 oct ! \ default octave
    engine-on init-board ;

\ UTILS
: rbit 0 1 randbits ;
: rrbit rbit rbit and ;


\ SEQUENCER STUFF
\ the pattern language from synth.tex
     
variable  time

macro
: .       exit ;        \ don't do anything, just exit jump table.
: o       bang exit ;   \ run 'bang' then exit jump table.

: pattern
    time @    \ behaviour depends on global 'time' variable
    #x0F and  \ to simplify, all patterns have length 16
    route ;

forth
  