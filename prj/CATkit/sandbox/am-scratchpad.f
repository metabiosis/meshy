\ aymeric's scratchbuttpookitty box

load sync.f

\ In case of crash, after pressing the reset button
\ restart can be used to re-init the board
: restart
    init-board
    engine-on
    ;

\ Simple mapping
\ Chromatic C major scale

\ : setoctave oct ! ;
: setoctave 3 + oct ! ;
    
: c  setoctave  0 note0 square note-sync silence ;
: c# setoctave  1 note0 square note-sync silence ;
: d  setoctave  2 note0 square note-sync silence ;
: d# setoctave  3 note0 square note-sync silence ;
: e  setoctave  4 note0 square note-sync silence ;
: f  setoctave  5 note0 square note-sync silence ;
: f# setoctave  6 note0 square note-sync silence ;
: g  setoctave  7 note0 square note-sync silence ;
: g# setoctave  8 note0 square note-sync silence ;
: a  setoctave  9 note0 square note-sync silence ;
: a# setoctave 10 note0 square note-sync silence ;
: b  setoctave 11 note0 square note-sync silence ;

\ Safe range seems to be 8 octaves from 0 to 7
: addup 1 + dup ;
: octaver for addup c next ;
: scaletest note-sync -1 8 octaver ;

\ Simple sequencing
\ : seq1 c# f# g# a# ;
\ : seq2 c  c# d# f# ;
: seq1 c# c# c# a# ;
: seq2 c c# a# a# ;

    
: count4
    1 + dup
    4 = if
	drop 0
    else
	0 drop
    then
    ;
    
: octashift 4 for count4 dup next ;
: loop1  octashift seq1 drop ;
: 4loop1 4 for loop1 next ;
: loop2  octashift seq2 drop ;
: 4loop2 4 for loop2 next ;
    
: song note-sync
    0 octashift drop 4loop1
    1 octashift drop 4loop2
    2 octashift drop 4loop1
    3 octashift drop 4loop2
    0 octashift drop 4loop2
    1 octashift drop 4loop2
    2 octashift drop 4loop1
    3 octashift drop 4loop1
    ;