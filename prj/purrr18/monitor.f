\ -*- forth -*-

\ this is an example forth file for the 18f1220 chip with serial
\ connection.


\ config bits
\ internal oscillator, watchdog disabled
#x30000000 org
 #x00 , #xC8 , #x0F , #x00 ,
 #x00 , #x80 , #x80 , #x00 ,
 #x03 , #xC0 , #x03 , #xE0 ,
 #x03 , #x40 ,

\ code
0 org

\ time
8000000 constant fosc
9600    constant baud

\ memory
#x80  constant stack-data
#x90  constant stack-control
#x00  constant allot-ram
#x200 constant allot-flash

path pic18              \ system path
path prj/purrr18        \ application path

load p18f1220.f         \ chip type
load monitor-serial.f   \ shared serial monitor code

: hello fsymbol purrr18 \ this should reflect system name (for later autoload)

: warm
    init-stacks     \ setup DS RS XS
    init-chip       \ setup outputs and clock
    init-serial     \ init serial port RX/TX logic


    \ begin #x5A transmit again

    
    interpreter ;   \ fall into interpreter



allot-flash org

    