
\ INPUT


macro
: init-ad-ports  #b00000110 ADCON1 ! ;  \ analog inputs AN0-AN8
forth

: amp-on  LATC 3 high ;
: amp-off LATC 3 low ;    


\ Record a single buffer worth of samples at Fosc/2^11 = 4.88kHz,
\ which is 2^4 lower than the PWM frequency Fosc/2^7
    
: sample>
    wait-sample
    0 ad@ ;
    
: record \ samples --    
    for
        sample> !a+
    next ;

    
  
\ OUTPUT    

\ make sure to always turn the speaker off, it gets a load of DC that's
\ larger than the transistor rating.
    
: spk-on  LATC 2 low ;
: spk-off LATC 2 high ;


: space 0 for 10 for next next ;
: beep for spk-on space spk-off space next ;


: pwm-on
    #b00001100 CCP1CON ! ;
: pwm-off
    0 CCP1CON !
    spk-off ;


\ We're running with pwm frequency of 78kHz. This gives us 7 bit
\ resolution. This word maps things so 0 = off / MIC. Again, be
\ careful with max PWM output!

: clip
    1st 7 high? if drop #x7F then ;    
: pwm
    clip
    #xFF xor
    #x7F and 1 +
    CCPR1L ! ;

: init-tx-timer  #b00000111 T2CON and! ;
: init-rx-timer  #b01111000 T2CON or! ;
    
    
: init-analog
    TRISC 2 low   \ pwm output
    TRISC 3 low   \ amp enable output
    amp-on
    spk-off
    init-ad-ports
    init-ad
    #x7F PR2 !
    tmr2-init
    0 pwm \ safe value
    ;


\ Time keeping

\ 78kHz  
: wait-sample
    begin tmr2-if high? until
    tmr2-if low
    ;

