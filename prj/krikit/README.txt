

to send out waves other than the ones provided for the data
transmission, you have to take this in mind:

  * start with 'tx-settings' to correctly recover from receive
    mode. this sets the timer correctly and initializes the
    oscillators.
    
  * ALWAYS switch the sound OFF when you're done using
    'ramp-off'. it's also best to use 'ramp-on' to prevent
    clicks. both words go from the speaker off condition, to the DC
    bias necessary to get appropriate signal swing.

  * sending out different frequencies. have a look at the 'envelope-1'
    word and 'init-tx-carrier' words in modulator.f which contain code
    to set the basic modulator parameters. an oscillator consists of a
    16bit phase value and a 16bit frequency value, both in 0.16 fixed
    point, representing a value between 0 and almost 1 (note that
    because of phase wrap-around and frequency aliasing, we have 1 ==
    0). the frequency is relative to the sampling frequency, which by
    default (for output) is equal to 78kHz.

  * to run a single tick of the ouput synth, use the word
    'modulator-tick'. this word uses the 8 bit fixed point value
    'attenuation' to provide sound volume.
    
see beeps.f for an example


