\ the RGB led is connected like this:

\ 26 RB5      o--[220]---o
\                        |
\ 25 RB4      o-----o    |
\                   |    |  
\ 24 RB3      o--o  G    |
\                B  |    |
\ 23 RB2/AN8  o--o--o----o
\                R
\ 22 RB1      o--o


\ in analog.f 23 RB2/AN8 is configured as analog input
\ : init-ad-ports  #b00000110 ADCON1 ! ;  \ analog inputs AN0-AN8

macro
: led-blue  LATB 3 ;
: led-red   LATB 1 ;
: led-green LATB 4 ;

: led-resistor LATB 5 ;
: led-anode    LATB 2 ;

: off-rgb   led-resistor low ;
: on-rgb    led-resistor high ;
    
forth

\ TRISB 2 needs to be one (input) even if configured as analog in. the
\ init code below assumes the port is configured as all input, and
\ will just set the appropriate output bits.
 
: init-rgb
    status-off \ want the other one off
    black
    \ 76543210
    #b11000101 TRISB and! ;
    
    
: red
    on-rgb
    led-red   low
    led-blue  high
    led-green high ;

: green
    on-rgb
    led-red   high
    led-blue  high
    led-green low ;

: blue
    on-rgb
    led-red   high
    led-blue  low
    led-green high ;

: black
    off-rgb ;


\ desired color values + S/D color accumulators
variable d-red    variable a-red
variable d-green  variable a-green
variable d-blue   variable a-blue

\ the S/D algorithm works as follows: add the desired color to the
\ accumulator, and output the carry flag during this phase.

macro
: led-update  var on |
    var @
    var 1 + +!  \ integrate
    c? if on compile exit then
    black ;
forth

\ LED control sequencing is a state machine
variable led-state

: drive-red    d-red   ' red   led-update ;
: drive-green  d-green ' green led-update ;
: drive-black  d-blue  ' blue  led-update ;

: led-next
    1 led-state +!
    led-state @ 3 and
    route
        drive-red ;    \ RED is less bright, do it twice
        drive-green ;
        drive-red ;
        drive-black ;

: led-duty
    4 for 0 for next next ;  \ simulate receiver frequency

: rgb
    d-blue !
    d-green !
    d-red ! ;

: color
    rgb
: glow
    begin
        led-next
        led-duty
    async.rx-ready? until ;


: led-demo
    0 0 0 rgb
    begin
        1 d-red -!
        2 d-green -!
        3 d-blue -!
        0 for
            led-next
            led-duty
        next
    async.rx-ready? until ;
        