\ meshy-sys.f

\ UTILS

: 2drop drop drop ;

\ DEBUG

\ LED
: blink-pause 0 for 0 for next next ;
: std-blink status-on blink-pause status-off ;
: red-blink red blink-pause black ;
: blue-blink blue blink-pause black ;
: green-blink green blink-pause black ;