\ misc util

macro    
: flip  #x80 xor ; \ convert to signed (flip sign bit)
forth

load lpf-s16.f

\ The data filters.
  
2variable data-I variable data-I-headroom  2variable data-I-coef
2variable data-Q variable data-Q-headroom  2variable data-Q-coef

: init-filters
    #x0300 lohi data-I lpf-init  \ init filters + coefficient
    #x0300 lohi data-Q lpf-init  \ a = 2^{-10} -> #x40 ... pick a little more
;    

\ takes signed input    
\ ul uh --      
: filter-I data-I lpf-tick ;
: filter-Q data-Q lpf-tick ;

\ The input mixer. After reading a byte from the a/d converter, it
\ will be multiplied with the locally synthesized carrier. It is
\ essential to not loose any bits at this stage.
    
2variable rx-carrier
2variable rx-carrier-inc    


\ Doesn't matter much here, since we either don't care about the phase
\ (AM) or will set it using a PLL (PM). So pick something simple.
: init-rx-phase
    #x0000 lohi rx-carrier 2! ;

\ sampling frequency is 8x higher than carrier (610 -> 5kHz), which
\ places the relative increment at 256/8 = 32

: zero-rx-carrier
    0 rx-carrier-inc !
    0 rx-carrier-inc 1 + ! ;
: bias-rx-carrier
    #x20 rx-carrier-inc 1 + +! ;

: init-rx-carrier
    zero-rx-carrier
    bias-rx-carrier ;


\ signed multiplication: x y -- rh rl
    
\ this is used instead of fractional multiplication: we deal with the
\ shift somewhere else. (TODO: can signed mul be eliminated?, maybe
\ 2-quadrant only?)
: s8x8->16
    s8x8->16/PROD
    PRODL @
    PRODH @ ;
    
\ This does signed multiplication in the core loop so that DC
\ compensation isn't necessary. The impact of this is about 10 cycles
\ in a 40 cycle core. Also, this uses a 'cos' LUT instead of reading
\ the coefficients from consecutive memory locations. That looses
\ about 40 more cycles. So there is definitely room for improvement,
\ though we're not really in trouble speed-wise.

: rx-I    rx-carrier osc-I ;
: rx-Q    rx-carrier osc-Q ;
: rx-inc  rx-carrier osc-inc ;   
    
: mix/filter \ signed.sample --
    \ dup !a+
    
    dup rx-I s8x8->16 filter-I
        rx-Q s8x8->16 filter-Q

    rx-inc ;

: init-rx    
    init-rx-phase
    init-rx-carrier
    init-start-threshold
    init-filters
    ;    

: samples   0 1 a!! ;
\ : processed 0 2 a!! ;    
        
2variable data-power      \ instantaneous signal power
    
: power
    data-I 1 + 2@ _dup _*s _2/
    data-Q 1 + 2@ _dup _*s _2/ _+  \ square of amplitude
    data-power 2!                  \ save instantaneous power
    ;
    

macro
: 2@debug var |
    var @ !a+ var 1 + @ flip !a+ ;
forth
    
: ook-debug
    data-power    2@debug
    ;

macro
: 2@record var |
    var @ !a+
    var 1 + @ !a+ ;
: symbol-begin 4 for 128 for ;
: symbol-end   next next ;
forth

\ process an input sample  
: rx-ook-tick  
    sample>
    flip \ all math is signed
    mix/filter
    power ;
   
\ process input for symbol duration + sample
: rx-ook-sample
    symbol-begin rx-ook-tick symbol-end
    data-power 2@record ;        

\ perform adaptive thresholding. simply differentiate the signal.


: 2a@+ @a+ @a+ ;
: 2a!+ >x !a+ x> !a+ ;


variable data-byte
variable stop-bit   \ 0 = valid (ON), 1 = invalid (OFF)

\ the default OOK start bit detection threshold and data bit
\ threshold. the latter is computed adaptively.
  
2variable threshold
2variable start-threshold

: init-start-threshold    
    #x0200 lohi start-threshold 2! ;

    
: a-thresh>c
    @a+ threshold     @ nop - drop
    @a+ threshold 1 + @ nop -- drop ;

: thresh-init
    \ get maximum sample
    @a+ threshold !
    @a+ threshold 1 + ! ;

: thresh-update
    a-thresh>c
    c? if
        2 al -!
        thresh-init
    then ;

: thresh-halve    
    \ halve it
    clc
    threshold 1 + rot>>c!
    threshold     rot>>c! ;

    
: rx-ook-compute-threshold
    samples
    thresh-init
    9 for
        thresh-update
    next
    thresh-halve  \ 1/4 of max
    thresh-halve
;
    
: rx-ook-compare-threshold \ -- databyte stopbits

    samples 2 al +! \ skip start bit

    0  \ data bit accumulator
    8 for
        a-thresh>c
        rot>>c
    next
    #xFF xor \ invert bits


    \ determine validity depending on stop bit. if carrier = ON
    \ (stopbit = 0) this byte is valid.
    
    a-thresh>c 0 rot<<c

\    a-thresh>c 0 rot<<c 1 xor and
    
    ;
    
: rx-ook-postproc \ -- byte valid
    rx-ook-compute-threshold
    rx-ook-compare-threshold ;


: done
    status-off
    10 for status-wait next
    status-on ;

\ wait for signal


    
: rx-ook-wait
    begin
        rx-ook-tick
        start-threshold     @ data-power -!
        start-threshold 1 + @ data-power 1 + --!
    data-power 1 + 7 low? until ;
        
: rx-ook-byte \ -- byte valid
    rx-ook-wait   \ wait until symbol comes in
    samples
    10 for rx-ook-sample next
    rx-ook-postproc ;


\ : test-rx-ook
\     samples
\     4 for
\         128 for \ this can be made variable to sync to symbol clock
\             4
\             8 *
\             for
\                 sample>
\                 \ dup !a+

\                 flip \ all math is signed
\                 mix/filter
\                 power

\             next
\             ook-debug
\         next
\     next ;
    
