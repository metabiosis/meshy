
\ load lpf.f

\ on-off keying demodulator


\ BAND PASS FILTER

\ A complex 1-pole, tuned around 1/8 Hz, with a decay of about
\ (1-2^(-8)). The idea is to make a simple implementation.

\ x += c (u - x)



\ To simplify the implementation, i'm switching to a 1/4 Hz carrier,
\ which means reducing the sample rate. This gives a very simple
\ rotation matrix, which can be combined with the byte-shift lowpass
\ filter.
