\ meshy-fuzzy.f

\ RANDOM WEIGHTS
\ 1 rnd -> 1/2 
\ 2 rnd -> 1/4 
\ 3 rnd -> 1/8
\ 4 rnd -> 1/16 
\ ... 
\ 8 rnd -> 1/256
: rnd 0 swap randbits ;

\ CONTAMINATION    
variable weakness
variable my-meme
variable new-meme
: genememe randbyte new-meme ! ;
: play-meme my-meme @ tx ;
: contaminate
    std-blink \ DEBUG!
    new-meme @ my-meme ! ;
: viral-attack 1 weakness +! ;
: new-reign 0 weakness ! ;
: contamination?
    0 = if
	contaminate
	new-reign
	\ green-blink \ DEBUG new meme installed
	led-contamination
    else
	viral-attack
	\ blue-blink \ DEBUG meme bounced
	led-bounce
    then
    ;

: force-contamination? 1 rnd contamination? ;

    
\ BEHAVIOUR

: test-contamination
    weakness @ 10 <? if
	10 weakness @ - >> 1 + rnd contamination?
    else
	weakness @ 12 <? if
	    force-contamination?
	else
	    contaminate
	    new-reign
	    \ green-blink \ DEBUG new meme installed
	    led-contamination
	then
	2drop
    then
    2drop
    ;
    
: decision
    \ play-meme \ current meme

    weakness @ 1 <? if
	viral-attack
	\ blue-blink \ DEBUG meme bounced !
	led-bounce
    else
	test-contamination
    then
    2drop
    ;

\ INIT

: init-fuzzy
    new-reign
    randbyte my-meme !
    ;


\ CRAP    
    
: ctm-loops for
	genememe \ a new meme enters
	decision \ do we accept it
    next ;

    \ rx
    \ rx/valid?