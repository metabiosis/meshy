
: reversals
    init-tx bpsk
    begin
        4 for 0 byte next \ plan carrier
        1 byte            \ reversal
    again ;


    

: main

    
: bpsk-msg
    init-tx bpsk
    begin
        ramp-on
        4 for 0 byte next \ plain carrier sync preamble
        #x1 byte         
        #x1 byte
        #x1 byte         
        #x1 byte
        1 for 0 byte next \ postamble
        ramp-off
        64 silence-symbols \ wait for 8 bytes
        
    again ;


    
: message
    init-tx bpsk
    begin
        ramp-on
        4 for 0 byte next \ plain carrier sync preamble
        #x55 byte         \ message (8 reversals)
        #x55 byte         \ message (8 reversals)
        ramp-off
        64 silence-symbols \ wait for 8 bytes
        
    again ;



\ load direct.f


\ variable testinc

\ 2variable testptr
\ : a>testptr  
\     al @ testptr !
\     ah @ testptr 1 + ! ;

\ : testptr>a
\     testptr @ al !
\     testptr 1 + @ ah ! ;

\ : dbuf
\     0 2 a!!
\     a>testptr
\     ;

\ : >dbuf
\     testptr>a
\     !a+
\     a>testptr ;
    

\ : lpftest
\     dbuf
\     init

\     0
    
\     2 for
\         0 for
\             dup cos
\             flip mix/filter
\             testinc @ +
\         next
\     next

\     drop ;


\ macro
\ : 2@ var | var @ var 1 + @ ;  
\ : negate #xFF xor 1 + ;
\ forth
    

    
\ macro
\ : lpf-abs lpf |
\     lpf 1 + @
\     lpf 2 + 7 high? if negate then ;
    
\ forth

\ : lpf-IQ
\     lpf-I lpf-abs
\     lpf-Q lpf-abs ;
    