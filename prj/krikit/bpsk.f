\ code from modulator.f for BPSK demodulator

\ i can't get it to work: carrier synchronizes nicely, but I
\ transitions kick the PLL out of sync. i suspect the problem has to
\ do with some sign or wraparound bug..

\ what does work:
\  * zero error carrier detect (integrator)

2variable ierror variable ierror-headrooom 2variable ierror-coef

: init-pll
    #x0010 lohi ierror lpf-init
    ;

\ not an LPF but an integrator    
: integrate-ierror ierror int-tick ;    

2variable error

macro
: pll-error-feedback var |
    var @      rx-carrier-inc -!
    var 1 + @  rx-carrier-inc 1 + --!
;    
forth


: __pll-compute-error  
    \ error = Q * sign(I)
    data-Q 1 + @  error !
    data-Q 2 + @  error 1 + !
    data-I 2 + 7 high? if
        #xFF error xor!     
        #xFF error 1 + xor!
    then ;

: pll-compute-error
    data-I 1 + 2@
    data-Q 1 + 2@ _*s

    \ save high byte + sign extend
    dup  error !
    sign error 1 + !
    drop
    ;

: sign
    rot<< 1 and
    #xFF xor 1 +
    ;
    
    
\ PLL takes feedback directly from (gain compensated) Q component.
: pll
    
    \ reset carrier frequency to expected
    init-rx-carrier

    pll-compute-error

    \ error integration
    error @
    error 1 + @
    integrate-ierror

    \ feedback:
    error      pll-error-feedback  \ proportional 
    ierror 1 + pll-error-feedback  \ integrating
    
    ;

: debug
    \ lpf-power >x !a+ x> flip !a+
            
    data-I 1 + @ !a+
    data-I 2 + @ flip !a+
    data-Q 1 + @ !a+
    data-Q 2 + @ flip !a+


\     error @ !a+
\     error 1 + @ flip !a+
    
\     ierror 1 + @ !a+
\     ierror 2 + @ flip !a+
    
\     rx-carrier-inc @ !a+
\     rx-carrier-inc 1 + @ flip !a+

    ;


\ run the filter/mixer in OOK mode: no synchronization

    \ 2variable ook-threshold  \ negative number
    
\ run the filter/mixer for one symbol length (= 512 taps)
: rx-symbol \ --
    samples
    4 for
        128 for \ this can be made variable to sync to symbol clock
            4
            8 *
            for
                sample>
                \ dup !a+

                flip \ all math is signed
                mix/filter
                pll
            next
            debug
        next
    next ;
    
    