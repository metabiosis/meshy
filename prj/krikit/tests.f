
\ load modulator.f



variable on-freq  
variable off-freq   



\ 3.27 ms  
: on-256 \ phase -- phase+
    0 for
        dup        
        cos
        att @ n>>   \ amplitude
        pwm
        wait-sample
        freq @ +    \ time increment
    next ;

: symbol
    freq !
    128 \ start phase
    dur @ for on-256 next drop ;

: 1wave
    for
        on-freq @ symbol
    next
    0 pwm ;

: 0wave
    for
        off-freq @ symbol
    next
    0 pwm ;
    
: 01wave
    for
        on-freq @ symbol
        off-freq @ symbol
    next
    0 pwm \ switch off!
    ;



\ this looks best:
\  Sat Nov 24 17:38:49 CET 2007   
\ init-analog pwm-on 4 dur ! 1 on-freq ! 2 off-freq ! 3 att ! 0 01wave


: testbleep    
    4 dur !
    1 on-freq !
    2 off-freq !
    3 att !
    #x40 01wave ;

: init
    init-analog
    pwm-on ;
    
: main
    init
    testbleep
    interpreter ;

macro
: install
    app-boot org-push main exit org-pop ;
forth
  