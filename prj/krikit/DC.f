\ The crudest possible receiver does simply power integration. In
\ order for this to have a DC reference, we need to use a DC offset
\ correction filter. DC depends on component tolerances, so it has to
\ be computed on the fly.

2variable DC-offset

: update-DC \ byte --
    DC-offset 1 + @ DC-offset      -!  \ decay
                  0 DC-offset 1 + --!
    
                    DC-offset      +!  \ add
                  0 DC-offset 1 + ++!
    ;

    

: init-DC
    128 DC-offset !
: collect-DC    
    0 for
        wait-sample
        0 ad@ update-DC
    next ;
: ncollect-DC
    for collect-DC next ;
        
